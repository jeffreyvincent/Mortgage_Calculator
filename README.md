# Mortgage Interest Calculator

## Instructions
In this project you will use HTML, CSS, and JavaScript to create a simple mortgage interest calculator app.
Make sure to follow the Eek style guidelines: https://gitlab.com/eekinc/style_guides
And the Eek code review guidelines: https://gitlab.com/eekinc/code-review

You will make use of the following:
[Slurp](https://github.com/jdsutton/Slurp) - For building you project in real time
[JSUtils](https://github.com/jdsutton/JSUtils) - For manipulating the DOM.
[JsRender](https://github.com/jdsutton/JsRender) - For rendering templates.

### Part 0: Setup
Clone and install this project:

```
git clone --recursive https://gitlab.com/eekinc/eek_labs.git
cd eek_labs
make init_submodules
make
```

### Part 1: Calculator
We want to calculate how much interest will accrue on a mortgage loan over a number of years.

1. Create a webpage with inputs for the loan principle, interest rate percentage, and number of years. Make sure to include clear labels for each.
2. Create a button labelled "Calculate", which we will use to calculate the interest.
3. Implement a function which reads the inputs from the page, computes interest ([continuous](http://www.meta-financial.com/lessons/compound-interest/continuously-compounded-interest.php)), and prints the result to the console. 
4. Create a merge request with your changes in a new repository and assign it to your supervisor to review.