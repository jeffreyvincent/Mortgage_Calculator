[[__top__/src/JSUtils/dom.js]]
[[__top__/src/JsRender/render.js]]


/**
 * Returns the total interest to be paid over the course of the loan. 
 * @param {Object} data -- Data includes the principal, lifetime , and the interest rate of the loan. 
 * @return {Number}
 */
function calculateLoan(data){  
    let P = Number(data.principal);
    let r = Number(data.interest);
    let t = Number(data.years);
    const amount = P * Math.exp(r * t);
    
    return giveAnswer(amount)
};

/**
 * Returns data -- Data includes the principal, lifetime , and the interest rate of the loan.
 * @return {Object}
 */
function getData(){
    const data = Dom.getDataFromForm("form");
    console.log(data);
    calculateLoan(data);
};

/**
 * Renders the template and inserts the html into the specified container.
 * @param {Number} amount - The total interest to be paid over the course of the loan.
 */
function giveAnswer(amount) {
    const rendered = render("myTemplate", {
      value1: (Math.round(amount / 100) * 100).toLocaleString()
    });

    Dom.setContents('result', rendered);
};
//test commit
