DIST=dist
SRC=src
SLURP=Slurp

default:
	@# Runs the web app for development.
	@make -s build_dev
	@rm -f .slurplog
	cd $(DIST) && python3 -m http.server 5000

init_submodules:
	git submodule update --init --recursive

build_dev: clean
	@# Fast build for development.
	#| $(SLURP)/envVars.py $(ENV)/dev_vars.json --fileExtensions=html,js,css  --injectversion=True \
	#
	@mkdir -p $(DIST)
	python3 $(SLURP)/fileMonitor.py $(SRC) --destinationPath=$(DIST) --ignore=static,images \
	| $(SLURP)/spit.py --fileExtensions=png,jpg,jpeg,svg \
	| $(SLURP)/fileLinker.py --fileExtensions=js,html,css --maxDepth=15 \
	| $(SLURP)/ignore.py node_modules \
	| $(SLURP)/spit.py &

clean:
	@rm -rf $(DIST)

docs:
	@# Creates JS documentation.
	@node_modules/documentation/bin/documentation.js build --format=html --output=documentation $(SRC)
